package com.example.app_global_news.rest_api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.app_global_news.R;
import com.example.app_global_news.model.ResponseApiArticles;
import com.example.app_global_news.view.OnArticlesResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiAdapter {

    private static RestApiAdapter rest_api_adapter = null;
    private NewsApi news_api;
    private GeneralNews general_news;
    Call<ResponseApiArticles> call;
    public int total_results;
    Activity activity;




    public RestApiAdapter(Call<ResponseApiArticles> call, Activity activity) {

        this.call = call;
        this.activity = activity;
    }

    public static RestApiAdapter getRestApiAdapter() {

        if (rest_api_adapter == null) {
            rest_api_adapter = new RestApiAdapter(BuildGsonDeserializerArticle());
        }
        return rest_api_adapter;
    }

    private RestApiAdapter(Gson gson) {
        BuiltRetrofitConection(gson);
    }

    private void BuiltRetrofitConection(Gson gson) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstantsUrl.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        news_api = retrofit.create(NewsApi.class);
        general_news = retrofit.create(GeneralNews.class);
    }

    public static Gson BuildGsonDeserializerArticle() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ResponseApiArticles.class, new ArticleDeserializer());
        return gsonBuilder.create();
    }

    public NewsApi getNewsApi() {
        return news_api;
    }

    public GeneralNews getGeneralNews() {
        return general_news;
    }

    public void ConectionRetrofit(final OnArticlesResponse callback) {

        call.enqueue(new Callback<ResponseApiArticles>() {

            @Override
            public void onResponse(Call<ResponseApiArticles> call, Response<ResponseApiArticles> response) {

                ResponseApiArticles responseApiArticles = response.body();

                if (responseApiArticles != null) {

                    total_results = responseApiArticles.getTotal_Results();
                    callback.Articles(response.body());

                } else {
                    onFailure(call, new Throwable());
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseApiArticles> call, Throwable t) {
                if (activity != null) {

                    AlertDialog.Builder alertDialogbuilder = new AlertDialog.Builder(activity);
                    alertDialogbuilder.setMessage(activity.getResources().getString(R.string.message_wrong_conection) + "\n\n" + t.getMessage())
                            .setCancelable(true)
                            .setPositiveButton(activity.getResources().getString(R.string.label_refresh_conection), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog title = alertDialogbuilder.create();
                    title.setTitle(activity.getResources().getString(R.string.title_alertdialog_wrongconection));
                    title.show();
                }

            }
        });
    }

    public int getTotal_results() {
        return total_results;
    }

}