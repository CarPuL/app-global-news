package com.example.app_global_news.rest_api;

import com.example.app_global_news.model.Articles;
import com.example.app_global_news.model.ResponseApiArticles;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ArticleDeserializer implements JsonDeserializer<ResponseApiArticles> {

    @Override
    public ResponseApiArticles deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

        Gson gson = new Gson();
        ResponseApiArticles responseApiArticles = gson.fromJson(json, ResponseApiArticles.class);
        JsonArray articlesResponseData = json.getAsJsonObject().getAsJsonArray(JsonKeys.ARTICLE);
        JsonPrimitive total_results = json.getAsJsonObject().getAsJsonPrimitive(JsonKeys.TOTAL_RESULTS);
        int total = total_results.getAsInt();
        responseApiArticles.setArticles(deserializerArticlesJson(articlesResponseData));
        responseApiArticles.setTotal_Results(total);
        return responseApiArticles;
    }

    private ArrayList<Articles> deserializerArticlesJson(JsonArray articlesResponseData) {

        ArrayList<Articles> articles = new ArrayList<>();

        for (int i = 0; i < articlesResponseData.size(); i++) {

            JsonObject ResponseArticleObject = articlesResponseData.get(i).getAsJsonObject();
            JsonObject source = ResponseArticleObject.getAsJsonObject(JsonKeys.SOURCE);

            String id;
            if (!source.get(JsonKeys.ID).isJsonNull()) {
                id = source.get(JsonKeys.ID).getAsString();
            } else {
                id = "No Data";
            }
            String name;
            if (!source.get(JsonKeys.NAME).isJsonNull()) {
                name = source.get(JsonKeys.NAME).getAsString();
            } else {
                name = "No Data";
            }
            String author;
            if (!ResponseArticleObject.get(JsonKeys.AUTHOR).isJsonNull()) {
                author = ResponseArticleObject.get(JsonKeys.AUTHOR).getAsString();
            } else {
                author = "No Data";
            }
            String title;
            if (!ResponseArticleObject.get(JsonKeys.TITLE).isJsonNull()) {
                title = ResponseArticleObject.get(JsonKeys.TITLE).getAsString();
            } else {
                title = "No Data";
            }
            String descripcion;
            if (!ResponseArticleObject.get(JsonKeys.DESCRPTION).isJsonNull()) {
                descripcion = ResponseArticleObject.get(JsonKeys.DESCRPTION).getAsString();
            } else {
                descripcion = "No Data";
            }

            String url;
            if (!ResponseArticleObject.get(JsonKeys.URL).isJsonNull()) {
                url = ResponseArticleObject.get(JsonKeys.URL).getAsString();
            } else {
                url = "No Data";
            }

            String urlToImage;
            if (!ResponseArticleObject.get(JsonKeys.URLTOIMAGE).isJsonNull()) {
                urlToImage = ResponseArticleObject.get(JsonKeys.URLTOIMAGE).getAsString();
            } else {
                urlToImage = "No Data";
            }

            String publishedAt;
            if (!ResponseArticleObject.get(JsonKeys.PUBLISHEDAT).isJsonNull()) {
                publishedAt = ResponseArticleObject.get(JsonKeys.PUBLISHEDAT).getAsString();
            } else {
                publishedAt = "No Data";
            }

            String content;
            if (!ResponseArticleObject.get(JsonKeys.CONTENT).isJsonNull()) {
                content = ResponseArticleObject.get(JsonKeys.CONTENT).getAsString();
            } else {
                content = "No Data";
            }
            Articles current_article = new Articles();
            current_article.setId(id);
            current_article.setName(name);
            current_article.setAuthor(author);
            current_article.setTitle(title);
            current_article.setDescription(descripcion);
            current_article.setUrl(url);
            current_article.setUrlToImage(urlToImage);
            current_article.setPublishedAt(publishedAt);
            current_article.setContent(content);
            articles.add(current_article);
        }
        return articles;
    }
}
