package com.example.app_global_news.rest_api;

import com.example.app_global_news.model.ResponseApiArticles;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeneralNews {

    @GET(ConstantsUrl.URL_GENERAL_NEWS + ConstantsUrl.URL_API_KEY)
    Call<ResponseApiArticles> getData(@Query("page") int page
            , @Query("pagesize") int page_size
            , @Query("q") String key_word
            , @Query("sources") String source
            , @Query("from") String from
            , @Query("to") String to
            , @Query("language") String language
            , @Query("sortBy") String sortBy);
}