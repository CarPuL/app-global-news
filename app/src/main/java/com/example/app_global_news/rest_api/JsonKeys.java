package com.example.app_global_news.rest_api;

public class JsonKeys {

    public static final String ARTICLE = "articles";
    public static final String SOURCE = "source";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String AUTHOR = "author";
    public static final String TITLE = "title";
    public static final String DESCRPTION = "description";
    public static final String URL = "url";
    public static final String URLTOIMAGE = "urlToImage";
    public static final String PUBLISHEDAT = "publishedAt";
    public static final String CONTENT = "content";
    public static final String TOTAL_RESULTS = "totalResults";
}
