package com.example.app_global_news.rest_api;

public class ConstantsUrl {


    public static final String URL_BASE = "https://newsapi.org/v2/";
    public static final String URL_API_KEY = "apiKey=69ad4384f170464aa44f5460f99249f8";
    public static final int MAX_RESPONSE = 25;

    //https://newsapi.org/v2/top-headlines?apiKey=69ad4384f170464aa44f5460f99249f8
    public static final String URL_TOP_NEWS_API = "top-headlines?";

    //https://newsapi.org/v2/everything?apiKey=69ad4384f170464aa44f5460f99249f8
    public static final String URL_GENERAL_NEWS = "everything?";

    public static final int size_page = 5;
}
