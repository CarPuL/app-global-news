package com.example.app_global_news;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class SelectedArticle extends AppCompatActivity {

    TextView textview_title_article;
    TextView textview_description_article;
    TextView textview_author_article;
    TextView textview_publicat_article;
    ImageView imageview_article;
    ImageView imageview_goback_mainactivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection_article);
        initViews();                //Inicializa los Views
        intentFromMainActivity();   //Método para recibir la información de MainActivity
        goBackToMainActivity();     //Método para regresar a Main Activity
    }

    private void initViews() {

        textview_title_article = findViewById(R.id.textview_title_article);
        textview_description_article = (TextView) findViewById(R.id.textview_descripcion_article);
        textview_author_article = findViewById(R.id.author_article);
        textview_publicat_article = findViewById(R.id.published_at);
        imageview_article = findViewById(R.id.imagen_thumbnail);
        imageview_goback_mainactivity = findViewById(R.id.image_backarrowlist);

    }

    private void intentFromMainActivity() {
        Bundle e = getIntent().getExtras();

        if (null != e) {
            textview_title_article.setText(e.getString("title"));
            textview_description_article.setText(e.getString("description") + "\n");
            Picasso.with(this).load(e.getString("thumbnail"))
                    .placeholder(R.drawable.diarynews_image)
                    .resize(450, 300)
                    .centerCrop()
                    .into(imageview_article);
            textview_author_article.setText(getResources().getString(R.string.label_author) + " " + e.getString("author"));
            textview_publicat_article.setText(getResources().getString(R.string.label_published_at) + " " + e.getString("publishedAt"));
        }
    }

    private void goBackToMainActivity() {
        imageview_goback_mainactivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}