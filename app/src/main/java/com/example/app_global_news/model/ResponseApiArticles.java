package com.example.app_global_news.model;


import android.util.Log;

import com.example.app_global_news.model.Articles;

import java.util.ArrayList;


public class ResponseApiArticles {

    ArrayList<Articles> articles;
    int total_Results;

    public ArrayList<Articles> getArticles() {

        return articles;
    }

    public void setArticles(ArrayList<Articles> articles) {

        this.articles = articles;
    }

    public int getTotal_Results() {
        return total_Results;
    }

    public void setTotal_Results(int total_Results) {
        this.total_Results = total_Results;
    }
}
