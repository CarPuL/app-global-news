package com.example.app_global_news.model;


public class Articles {

    private String id;
    private String name;
    private String author;
    private String title;
    private String description;
    private String url;
    private String url_to_image;
    private String published_at;
    private String content;


    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUrlToImage(String url_to_image) {
        this.url_to_image = url_to_image;
    }

    public void setPublishedAt(String publishedAt) {
        this.published_at = publishedAt;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return url_to_image;
    }

    public String getPublishedAt() {
        return published_at;
    }

    public String getContent() {
        return content;
    }

}


