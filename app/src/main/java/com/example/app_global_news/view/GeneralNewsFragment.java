package com.example.app_global_news.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.app_global_news.model.Articles;
import com.example.app_global_news.R;
import com.example.app_global_news.rest_api.ConstantsUrl;
import com.example.app_global_news.rest_api.GeneralNews;
import com.example.app_global_news.model.ResponseApiArticles;
import com.example.app_global_news.rest_api.RestApiAdapter;
import com.example.app_global_news.view.SpinnerFilters.SettingSpinner;

import java.util.ArrayList;

import retrofit2.Call;

import static android.content.ContentValues.TAG;
import static com.example.app_global_news.rest_api.ConstantsUrl.size_page;

public class GeneralNewsFragment extends Fragment {

    private AdapterRecycler adapter;
    public static RecyclerView recyclerView;
    private ArrayList<Articles> list_articles = new ArrayList<>();
    private View v;
    public static SwipeRefreshLayout swipe_refresh_article;
    public int number_page = 1;
    public String key_word = "a";
    public String source = "";
    public String from = "";
    public String to = "";
    public String language = "";
    public String sortBy = "";
    private Call<ResponseApiArticles> call;
    private static GeneralNews generalNews = RestApiAdapter.getRestApiAdapter().getGeneralNews();
    private RestApiAdapter Conection;
    public static Activity activity;
    FrameLayout frameLayout_date_from;
    FrameLayout frameLayout_date_to;

    public GeneralNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_general_news_, container, false);
        activity = getActivity();
        recyclerView = v.findViewById(R.id.recycleView_generalNews);
        swipe_refresh_article = v.findViewById(R.id.swipe_list_article);
        frameLayout_date_from = activity.findViewById(R.id.framelayout_date_from);
        frameLayout_date_to = activity.findViewById(R.id.framelayout_date_to);
        recyclerView.addOnScrollListener(ScrollRecyclerArticles);
        SwipeRefreshListArticle();
        SettingStartConection();

        //ConectionRetrofit_GeneralNews(v);

        return v;
    }

    public void ConectionRetrofit_GeneralNews(final View v) {
/*
        call.enqueue(new Callback<ResponseApiArticles>() {

            @Override
            public void onResponse(Call<ResponseApiArticles> call, Response<ResponseApiArticles> response) {

                ResponseApiArticles responseApiArticles = response.body();

                if (responseApiArticles != null) {

                    if (adapter == null) {
                        list_articles = responseApiArticles.getArticles();
                        total_results = responseApiArticles.getTotal_Results();
                        adapter = new AdapterRecycler(list_articles, getActivity());
                        SettingAdapter();

                    } else {
                        next_articles = responseApiArticles.getArticles();
                        list_articles.addAll(next_articles);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    onFailure(call, new Throwable());
                    return;
                }
            }

            @Override
            public void onFailure(Call<ResponseApiArticles> call, Throwable t) {
                AlertDialog.Builder alertDialogbuilder = new AlertDialog.Builder(getActivity());
                alertDialogbuilder.setMessage(getResources().getString(R.string.message_wrong_conection) + "\n\n" + t.getMessage())
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.label_refresh_conection), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                AlertDialog title = alertDialogbuilder.create();
                title.setTitle(getResources().getString(R.string.title_alertdialog_wrongconection));
                title.show();
            }
        });*/
    }

    private RecyclerView.OnScrollListener ScrollRecyclerArticles = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (isLastArticleDisplayed(recyclerView)) {

                if ((number_page * size_page) >= ConstantsUrl.MAX_RESPONSE || (number_page * size_page) >= Conection.getTotal_results()) {
                    Toast.makeText(activity, "MAX RESPONSE: " + ConstantsUrl.MAX_RESPONSE
                            + "\n" + "TOTAL RESULTS: " + Conection.getTotal_results(), Toast.LENGTH_SHORT).show();

                } else {
                    number_page++;
                    //ConectionRetrofit_GeneralNews(v);
                    call = generalNews.getData(number_page, size_page, key_word, source, from, to, language, sortBy);
                    Conection = new RestApiAdapter(call, activity);
                    Conection.ConectionRetrofit(new OnArticlesResponse() {
                        @Override
                        public void Articles(ResponseApiArticles ilist) {
                            list_articles.addAll(ilist.getArticles());
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
            swipe_refresh_article.setRefreshing(false);
        }
    };

    private boolean isLastArticleDisplayed(RecyclerView recyclerView) {

        if (recyclerView.getAdapter().getItemCount() != 0) {

            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
                    .findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION &&
                    lastVisibleItemPosition == recyclerView.getAdapter().getItemCount()-3) {
                swipe_refresh_article.setRefreshing(true);
                return true;
            }
        }
        return false;
    }

    private void SwipeRefreshListArticle() {
        swipe_refresh_article.setProgressBackgroundColor(R.color.color_accent);
        swipe_refresh_article.setProgressViewOffset(true, getResources().getDimensionPixelOffset(R.dimen.refresher_offset)
                , getResources().getDimensionPixelOffset(R.dimen.refresher_offset_end));
        swipe_refresh_article.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                number_page = 1;
                SettingStartConection();
                swipe_refresh_article.setRefreshing(false);
                Toast.makeText(activity, "Refresh on GeneralNews ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SettingAdapterRecycler() {
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
    }

    public void SettingStartConection() {

        call = generalNews.getData(number_page, size_page, key_word, source, from, to, language, sortBy);
        Conection = new RestApiAdapter(call, getActivity());
        Conection.ConectionRetrofit(new OnArticlesResponse() {
            @Override
            public void Articles(ResponseApiArticles ilist) {
                list_articles = ilist.getArticles();
                adapter = new AdapterRecycler(list_articles, activity);
                SettingAdapterRecycler();
            }
        });
        recyclerView.addOnScrollListener(ScrollRecyclerArticles);
    }
}