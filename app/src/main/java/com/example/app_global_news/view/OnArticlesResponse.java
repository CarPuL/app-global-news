package com.example.app_global_news.view;

import com.example.app_global_news.model.ResponseApiArticles;

public interface OnArticlesResponse {

    void Articles(ResponseApiArticles ilist);
}
