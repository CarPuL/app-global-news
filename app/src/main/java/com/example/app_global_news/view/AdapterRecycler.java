package com.example.app_global_news.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_global_news.model.Articles;
import com.example.app_global_news.R;
import com.example.app_global_news.SelectedArticle;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class AdapterRecycler extends RecyclerView.Adapter<AdapterRecycler.ArticlesViewHolder> implements View.OnClickListener {

    private ArrayList<Articles> articles;
    private View.OnClickListener listener;
    private Activity activity;

    public AdapterRecycler(ArrayList<Articles> articles, Activity activity) {
        this.articles = articles;
        this.activity = activity;
    }

    @Override
    public ArticlesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recicler_style_view, viewGroup, false);
        v.setOnClickListener(this);
        ArticlesViewHolder articlesViewHolder = new ArticlesViewHolder(v);
        return articlesViewHolder;
    }

    @Override
    public void onBindViewHolder(ArticlesViewHolder articlesViewHolder, int pos) {

        final Articles item = articles.get(pos);
        articlesViewHolder.bindArticles(item);

        articlesViewHolder.tv_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG, "onClick: " + activity);
                Intent intent = new Intent(activity, SelectedArticle.class);
                Bundle bundle = new Bundle();
                bundle.putString("title", item.getTitle());
                bundle.putString("description", item.getDescription());
                bundle.putString("thumbnail", item.getUrlToImage());
                bundle.putString("author", item.getAuthor());
                bundle.putString("publishedAt", item.getPublishedAt());
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        if (articles != null) {
            return articles.size();
        } else {
            return 0;
        }
    }


    @Override
    public void onClick(View v) {
        if (listener != null)
            listener.onClick(v);
    }

    public static class ArticlesViewHolder extends RecyclerView.ViewHolder {

        public ImageView image_article;
        public TextView tv_source;
        public TextView tv_title;
        public TextView tv_url;


        public ArticlesViewHolder(View itemView) {
            super(itemView);

            tv_source = itemView.findViewById(R.id.textview_source);
            tv_title = itemView.findViewById(R.id.textview_title_list_articles);
            tv_url = itemView.findViewById(R.id.textview_url);
            image_article = itemView.findViewById(R.id.image_list_article);
        }

        public void bindArticles(Articles item) {

            tv_source.setText("Source: " + item.getName());
            tv_title.setText(item.getTitle() + "\n");
            tv_url.setText(item.getUrl());

            if (!item.getUrlToImage().equals("")) {
                Picasso.with(itemView.getContext()).load(item.getUrlToImage())
                        .placeholder(R.drawable.diarynews_image)
                        .resize(450, 350)
                        .centerCrop()
                        .into(image_article);
            } else {
                image_article.setImageResource(R.drawable.diarynews_image);
            }
        }
    }

}
