package com.example.app_global_news;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.app_global_news.view.GeneralNewsFragment;
import com.example.app_global_news.view.MainNewsFragment;
import com.example.app_global_news.view.PageAdapter;
import com.example.app_global_news.view.SpinnerFilters.SettingSpinner;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, AdapterView.OnItemSelectedListener, DrawerLayout.DrawerListener {

    private Toolbar toolbar;
    private TabLayout tablayout;
    private ViewPager view_pager;
    private Drawable icon_menu_over_flow;
    private Drawable icon_menu_navigation;
    private PageAdapter adapter;
    private FloatingActionButton fab_search_navigation;
    private FloatingActionButton fab_search_activity_main;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FrameLayout frameLayout_date_from;
    private FrameLayout frameLayout_date_to;
    private EditText edit_text_filter_from;
    private EditText edit_text_filter_to;
    private EditText edit_text_key_word;
    private Spinner spinner_country;
    private Spinner spinner_category;
    private Spinner spinner_source;
    private Calendar calendar = Calendar.getInstance();
    private final int month = calendar.get(Calendar.MONTH);
    private final int day = calendar.get(Calendar.DAY_OF_MONTH);
    private int year = calendar.get(Calendar.YEAR);
    private static final String ZERO = "0";
    private static final String BAR = "/";
    private String from = "", to = "", date = "";
    private ArrayList<String> arrayList_values_filters;
    private SettingSpinner settingSpinner;
    private MainNewsFragment mainNewsFragment = new MainNewsFragment();
    private GeneralNewsFragment generalNewsFragment = new GeneralNewsFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitViews();
        setUpViewPager();
        MakeNavigationViewDrawer();
    }

    public void InitViews() {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        tablayout = findViewById(R.id.tablayout);
        view_pager = findViewById(R.id.view_pager);
        edit_text_filter_from = findViewById(R.id.edit_text_filter_from);
        edit_text_filter_to = findViewById(R.id.edit_text_filter_to);
        edit_text_key_word = findViewById(R.id.edit_key_word);
        frameLayout_date_from = findViewById(R.id.framelayout_date_from);
        frameLayout_date_to = findViewById(R.id.framelayout_date_to);
        fab_search_navigation = findViewById(R.id.fab_search_navigation_drawer);
        fab_search_activity_main = findViewById(R.id.fab_search_activity_main);
        spinner_country = findViewById(R.id.spinner_filter_country);
        spinner_category = findViewById(R.id.spinner_filter_category);
        spinner_source = findViewById(R.id.spinner_filter_source);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        icon_menu_over_flow = getResources().getDrawable(R.drawable.ic_arrow_24dp);
        edit_text_filter_from.setOnClickListener(this);
        edit_text_filter_to.setOnClickListener(this);
        fab_search_navigation.setOnClickListener(this);
        fab_search_activity_main.setOnClickListener(this);
        spinner_source.setOnItemSelectedListener(this);
        drawer.setDrawerListener(this);
    }

    private void setUpViewPager() {
        adapter = new PageAdapter(getSupportFragmentManager(), addFragments());
        view_pager.setAdapter(adapter);
        tablayout.setupWithViewPager(view_pager);
        tablayout.getTabAt(0).setText(R.string.tab_0)
                .setIcon(R.drawable.ic_tab1_24dp);
        tablayout.getTabAt(1).setText(R.string.tab_1)
                .setIcon(R.drawable.ic_tab2_24dp);
    }

    //Navigation View Drawer
    private void MakeNavigationViewDrawer() {
        icon_menu_navigation = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_menu_nav_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.about_message, R.string.app_name);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(icon_menu_navigation);
        settingSpinner = new SettingSpinner(this);
    }

    private ArrayList<Fragment> addFragments() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new MainNewsFragment());
        fragments.add(new GeneralNewsFragment());

        return fragments;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        icon_menu_over_flow = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_menu);
        toolbar.setOverflowIcon(icon_menu_over_flow);
        getMenuInflater().inflate(R.menu.general_menu, menu);
        return true;
    }

    //Click on Menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_setting:

                break;
            case R.id.menu_about:

                AlertDialog.Builder alertDialogbuilder = new AlertDialog.Builder(this);
                alertDialogbuilder.setMessage(R.string.about_message)
                        .setCancelable(true)
                        .setPositiveButton(R.string.button_alertdialog_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                AlertDialog title = alertDialogbuilder.create();
                title.setTitle(R.string.menu_about);
                title.show();

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    //built-datePicker //Click on DatePicker
    public String OnclickImageViewFrom(final int edit_text) {

        DatePickerDialog getDate = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                final int currentMonth = month + 1;
                String day = (dayOfMonth < 10) ? ZERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                String monthFormat = (currentMonth < 10) ? ZERO + String.valueOf(currentMonth) : String.valueOf(currentMonth);
                EditText editText = findViewById(edit_text);
                editText.setText(day + BAR + monthFormat + BAR + year);
                date = day + BAR + monthFormat + BAR + year;
            }
        }, year, month, day);
        //Muestro el widget
        getDate.show();

        return date;
    }//end Onclick

    @Override //get Date //Click on Views
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.edit_text_filter_from:
                from = OnclickImageViewFrom(v.getId());
                break;
            case R.id.edit_text_filter_to:
                to = OnclickImageViewFrom(v.getId());
                break;
            case R.id.fab_search_navigation_drawer:

                arrayList_values_filters = settingSpinner.getValuesSpinner(this);

                if (tablayout.getSelectedTabPosition() == 0) {

                    SettingParametersRequestTopHeadLines();
                    mainNewsFragment.SettingStartConection();

                } else {

                    SettingParametersRequestGeneralNews();
                    generalNewsFragment.SettingStartConection();
                }

                break;
            case R.id.fab_search_activity_main:

                drawer.openDrawer(navigationView);
                break;

            default:
                break;
        }

    }

    public void SettingParametersRequestTopHeadLines() {

        mainNewsFragment.number_page = 1;
        mainNewsFragment.key_word = edit_text_key_word.getText().toString();
        mainNewsFragment.country = arrayList_values_filters.get(0);
        mainNewsFragment.category = arrayList_values_filters.get(1);
        mainNewsFragment.source = arrayList_values_filters.get(2);
        mainNewsFragment.language = arrayList_values_filters.get(3);

        if (arrayList_values_filters.get(0).equals("") && arrayList_values_filters.get(1).equals("") &&
                arrayList_values_filters.get(2).equals("") && arrayList_values_filters.get(3).equals("") &&
                edit_text_key_word.getText().toString().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.message_filter_topheadlines), Toast.LENGTH_SHORT).show();
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void SettingParametersRequestGeneralNews() {

        generalNewsFragment.number_page = 1;
        generalNewsFragment.key_word = edit_text_key_word.getText().toString();
        generalNewsFragment.source = arrayList_values_filters.get(2);
        generalNewsFragment.language = arrayList_values_filters.get(3);
        generalNewsFragment.sortBy = arrayList_values_filters.get(4);
        generalNewsFragment.from = from;
        generalNewsFragment.to = to;

        if (arrayList_values_filters.get(2).equals("") && edit_text_key_word.getText().toString().equals("")) {
            Toast.makeText(this, getResources().getString(R.string.message_filter_generalnews), Toast.LENGTH_SHORT).show();
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void SettingFieldsFilters() {

        if (tablayout.getSelectedTabPosition() == 0) {
            frameLayout_date_from.setVisibility(View.INVISIBLE);
            frameLayout_date_to.setVisibility(View.INVISIBLE);
            if (arrayList_values_filters.get(2).equals("")) {
                spinner_country.setVisibility(View.VISIBLE);
                spinner_category.setVisibility(View.VISIBLE);
            } else {
                spinner_country.setVisibility(View.INVISIBLE);
                spinner_category.setVisibility(View.INVISIBLE);
            }
        } else {
            frameLayout_date_from.setVisibility(View.VISIBLE);
            frameLayout_date_to.setVisibility(View.VISIBLE);
            spinner_country.setVisibility(View.INVISIBLE);
            spinner_category.setVisibility(View.INVISIBLE);
        }
    }

    @Override //click on Spinner
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        arrayList_values_filters = settingSpinner.getValuesSpinner(this);
        SettingFieldsFilters();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onDrawerSlide(@NonNull View view, float v) {
        SettingFieldsFilters();
    }

    @Override
    public void onDrawerOpened(@NonNull View view) {
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {
    }

    @Override
    public void onDrawerStateChanged(int i) {
    }
}